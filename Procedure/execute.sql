-- No.1.
DROP procedure if exists Name_Search;
DELIMITER $$
CREATE procedure Name_Search(IN name varchar(100)) 
BEGIN
	if name is null
		then select("You must supply a name")as Message_1;
	else if name not in(select p.pname from players p)
		then select("Name was not found") as Message_2; 
	else select p.pname as player, t.tname as team from teams t, players p 
		where p.team_id= t.tnum and p.pname=name; 
	end if;
    end if;
END $$
DELIMITER ;

CALL Name_search('Liam Messam');
CALL Name_search('Te Taka Keegan');
CALL Name_search(null);

-- No.2.
DROP procedure if exists Grinning_Coaches
DELIMITER $$
CREATE procedure Grinning_Coaches(IN round int)
BEGIN
	if round is null
		then select('You must supply a round number') as Message1;
	else if round<1 or round>11
		then select('You must enter a valid round number (1-11)')as Message2; 
	else
		select t.coach from teams t, results r where t.tnum = r.hometeam 
		and r.homescore>r.awayscore and r.roundnumber= round
			union
		select t.coach from teams t, results r where t.tnum = r.awayteam
		and r.homescore<r.awayscore and r.roundnumber= round;
	end if;
    end if;
END $$
DELIMITER ;

CALL Grinning_Coaches(5);
CALL Grinning_Coaches(12);
CALL Grinning_Coaches(null);

-- -- No.3.
DROP PROCEDURE Player_Numbers
DELIMITER $$
CREATE procedure Player_Numbers(IN tname varchar(30), OUT pnum int)
BEGIN
	IF tname is null
		then select('You must enter one of the team names')as Message1;
	ELSE IF tname not in(select t.tname from teams t)
		then select('You must enter one of the team names')as Message2;
    ELSE
		select count(p.pname) 
		into pnum
		from teams t, players p where t.tnum = p.team_id
		and t.tname= tname;
	END IF;
    END IF;
END $$
DELIMITER ;

CALL Player_Numbers('Blues', @pnum);
CALL Player_Numbers('Greens', @pnum);
CALL Player_Numbers(NULL, @pnum);
select @pnum;






























